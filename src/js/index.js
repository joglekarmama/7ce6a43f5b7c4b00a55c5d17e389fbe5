import Phaser from "phaser";
import Game1 from "./Game1";
import PreGame from "./PreGame";
import PostGame from "./PostGame";
import Instructions from "./Instructions";
import LeaderBoard from "./Leaderboard";
import PostLeaderBoard from "./PostLeaderBoard";
import config from "visual-config-exposer";

const body = document.querySelector("body");
body.style.backgroundImage = `url(${config.preGameScreen.BackgroundImage})`;
body.style.backgroundRepeat = "no-repeat";
body.style.backgroundSize = "cover";

const CONFIG = {
  type: Phaser.AUTO,
  backgroundColor: "#000000",
  parent: "phaser-example",
  scale: {
    mode: Phaser.Scale.RESIZE,
    width: 740,
    height: 900,
    min: {
      width: window.innerWidth, //320,
      height: window.innerHeight, //480,
    },
    max: {
      width: window.innerWidth,
      height: window.innerHeight,
    },
  },
  dom: {
    createContainer: true,
  },
  /*scene: [PreGame, Game, PostGame, Level2, Level3, Instructions, LeaderBoard, PostLeaderBoard],*/
  scene: [PreGame, Game1, PostGame, Instructions, LeaderBoard, PostLeaderBoard],
  physics: {
    default: "arcade",
    arcade: {
      gravity: { y: 300 },
      // debug: true,
      // debugShowBody: true,
      // debugShowStaticBody: true,
    },
  },
};

let game = new Phaser.Game(CONFIG);
